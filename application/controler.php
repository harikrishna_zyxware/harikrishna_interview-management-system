<?php

function home_view(){

    require 'templates/home.tpl.php';

} 
function edit_interview(){

    require 'templates/int_edit.tpl.php';

}
function add_interview(){

    add_inter();
    require 'templates/home.tpl.php';
   // header('Location:templates/home.tpl.php');
   // exit;
}
function edit_participants(){

    require 'templates/part_edit.tpl.php';

}
function add_part(){

    add_candidate();
    require 'templates/home.tpl.php';
   // header('Location:templates/home.tpl.php');
   // exit;

}
function add_rounds(){

    require 'templates/round_add.tpl.php';

}
function add_round(){

    round_add();
    require 'templates/home.tpl.php';
    //header('Location:templates/home.tpl.php');
    //exit;

}
function view_mark_func(){

  $round = get_rounds(); 
  $names = get_names();
  $mark = get_marks();
  require 'templates/marks.tpl.php';

}
function add_mark_func(){
    
  $round = get_rounds();  
  require 'templates/edit_mark.tpl.php';

}
function insert_mark(){
  if($_SESSION['total'] == $_POST['captcha']){
    add_mark();
    session_destroy();
    view_mark_func();
  }
  else {
      session_destroy();
      echo "<script>alert('Wrong Captcha!!!!!')</script>";
      //require 'templates/edit_mark.tpl.php';
      add_mark_func();

  }
  //header('Location:templates/marks.tpl.php');

}
function prepopulate(){
    $num1 = rand(1, 9);
    $num2 = rand(1, 9);
    $total = operation($num1, $num2) ;
    $_SESSION['total'] = $total;
    captcha_img($num1, $num2);
    $round = get_rounds(); 
    $row = get_details();
    require 'templates/edit_mark.tpl.php';
}
function operation($num1, $num2){
  $operation = array('+', '-', '*');
  $op = rand(1, 3);
  $_SESSION['op'] = $operation[$op-1];
  switch($op){
      case 1: $res = $num1 + $num2;
              break;
      case 2: $res = $num1 - $num2;
              break;
      case 3: $res = $num1 * $num2;
              break;
  }
  return $res;
}
function captcha_img($num1, $num2){
  $im = @imagecreate(150, 40)
     or die("Cannot Initialize new GD image stream");
  $string = $num1 . ' '.$_SESSION['op']. ' ' . $num2; 
  $background_color = imagecolorallocate($im, 255, 255, 255);
  $text_color = imagecolorallocate($im, 233, 14, 91);
  imagestring($im, 5, 10, 10,  $string, $text_color); 
  ob_start();
  imagepng($im);
  $imagstr = ob_get_contents();
  ob_clean();
  $file = fopen("templates/images/captcha.png","w+") or die("file open failed");
  fwrite($file,$imagstr);
  fclose($file); 
  imagedestroy($im);
}
?>