<?php 
//include model and controller

//starting session for math captcha
session_start();

require_once 'model.php';
require_once 'controler.php';

$uri = parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH);

if("/interview/" == $uri) {
  //home view of for buttons;
  home_view();

} 
elseif("/interview/index.php/interview_details" == $uri) {
  
  edit_interview();

} 
elseif("/interview/index.php/add_interview" == $uri) {

  add_interview();

} 
elseif("/interview/index.php/participants" == $uri) {
    
    edit_participants();

} 
elseif("/interview/index.php/add_part" == $uri) {

    add_part();

} 
elseif("/interview/index.php/add_rounds" == $uri){

    add_rounds();

} 
elseif("/interview/index.php/add_round" == $uri){

    add_round();

}
elseif("/interview/index.php/marks" == $uri){

    view_mark_func();

}
elseif("/interview/index.php/list" == $uri && isset($_GET['id'])){

    prepopulate();

}
elseif("/interview/index.php/list" == $uri){

    add_mark_func();

}

elseif("/interview/index.php/edit_marks" == $uri){

    insert_mark();

}

?>