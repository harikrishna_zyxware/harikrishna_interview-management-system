<?php ob_start()?>
<!-- interview deediting  -->
  <div class = "form_box">
    <form id="inter_form" action="/interview/index.php/add_interview" method="POST" >
      <label for="name">Interview title :</label>
      <input type = "text" placeholder="Title" name ="title"></input>
      <label for="location">Interview Location :</label>
      <input type = "text" placeholder="Location" name ="location"></input>
      <label for="start_date">Starting date :</label>
      <input type = "text" placeholder="yyyy-dd-mm" name ="start_date"></input>
      <label for="end_date">Ending date :</label>
      <input type = "text" placeholder="yyyy-dd-mm" name ="end_date"></input>
      <input type = "submit" name="submit"></input>
    </form>
  </div>
<?php $content = ob_get_clean()?>
<?php include 'templates/layout.tpl.php';?>