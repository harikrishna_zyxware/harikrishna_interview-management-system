<!DOCTYPE html>
<html>
  <head>
    <title>Interview</title>
    <link href="/interview/templates/css/style.css" rel="stylesheet" >
    <link href="/interview/templates/css/bootstrap.css" rel="stylesheet">
  </head>
  <body>
    <div class="header">
      <div class="list" >
        <ul>
          <li><a href="http://blog.local/interview/">HOME</a></li>
          <li><a href="http://blog.local/interview/">About Us</a></li>
          <li><a href="http://blog.local/interview/">Contact Us</a></li>
        </ul>
      </div>
    </div>
    <div class="main">
      <div class="box">
        <?php echo $content ?>
      </div>
    </div>
    <div class="footer">
      <div class="list">
      </div>
    </div>
  </body>
</html>