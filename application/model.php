<?php
// model.php
function create_database_connection() {
  $server = "localhost";
  $database = "interview";
  $username = 'root';
  $password = 'user0123';

  $link = new PDO("mysql:host=$server; dbname=$database", $username, $password);
  return $link;
}
function database_destroy($link){

  $link = null;

}
function add_inter(){
  
  $link = create_database_connection();
  $query = "INSERT INTO interview_details (`title`, `location`, `start_date`, `end_date`) VALUES (:title, :location, :start_date, :end_date)";
  $stmt = $link->prepare($query);
  $title = $_POST['title'];
  $loc = $_POST['location'];
  $strt = $_POST['start_date'];
  $enddat = $_POST['end_date'];
  $stmt->bindParam(':title', $title);
  $stmt->bindParam(':location', $loc);
  $stmt->bindParam(':start_date', $strt);
  $stmt->bindParam(':end_date', $enddat);

  $stmt->execute();

  database_destroy($link);

}
function add_candidate(){
  // Insert values into participant table
  $link = create_database_connection();
  $query = "INSERT INTO participant (`reg_id`, `name`, `email_id`, `phone`, `dob`, `address`) VALUES (:reg_id, :name, :email, :phone, :dob, :address)";
  $stmt = $link->prepare($query);
  $stmt->bindParam(':reg_id', $_POST['regid']);
  $stmt->bindParam(':name', $_POST['name']);
  $stmt->bindParam(':email', $_POST['email']);
  $stmt->bindParam(':phone', $_POST['phone']);
  $stmt->bindParam(':dob', $_POST['dob']);
  $stmt->bindParam(':address', $_POST['address']);
  $stmt->execute();
  
  //insert values into work experience table
  $id = $link->lastInsertId();
  $query = "INSERT INTO work_exp (`participant_id`, `company_name`, `leaving_reason`) VALUES (:participant_id, :company, :reason)";
  $stmt = $link->prepare($query);
  $stmt->bindParam(':participant_id', $id);
  $stmt->bindParam(':company', $_POST['company']);
  $stmt->bindParam(':reason', $_POST['reason']);
  $stmt->execute();

  //insert values into acadamic table
  $query = "INSERT INTO acadamic (`participant_id`, `course`, `percent`, `maths`, `passout`) VALUES (:participant_id, :course, :percent, :maths, :passout)";
  $stmt = $link->prepare($query);
  $stmt->bindParam(':participant_id', $id);
  $stmt->bindParam(':course', $_POST['course']);
  $stmt->bindParam('percent', $_POST['percent']);
  $stmt->bindParam(':maths', $_POST['maths']);
  $stmt->bindParam(':passout', $_POST['passout']);
  $stmt->execute();

  database_destroy($link);
}

function round_add(){

  //adding rounds 
  $link = create_database_connection();
  $query = "INSERT INTO round_details (`title`,`max_mark`) VALUES (:title,:mark)";
  $stmt = $link->prepare($query);
  $stmt->bindParam(':title', $_POST['title']);
  $stmt->bindParam(':mark', $_POST['max_mark']);
  $stmt->execute();
  database_destroy($link);

}

function get_rounds() {
  
  // getting details from rounds
  $link = create_database_connection();
  $stmt = $link->query("SELECT * FROM round_details");
  $result=array();
  database_destroy($link);
  while($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
      
      $result[]=$row;
  
  }
  return $result;
}

function get_names() {

  //geting participant details
  $link = create_database_connection();
  $stmt = $link->query("SELECT * FROM participant");
  $results = array();
  while($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
    $results[] = $row;
  }
  database_destroy($link);
  return $results;
}

function add_mark() {

  // adding marks to database
  $query = "INSERT INTO round_marks(`interview_title`,`round_id`,`participant_id`,`mark`,`comments`) 
  VALUES(:int_title,:roundid,:pid,:mark,:comments)";
  
  $query1 = "UPDATE `round_marks` SET `interview_title`=:int_title,`comments`=:comments,`mark`=:mark WHERE `participant_id`=:pid AND `round_id`=:roundid";
  $link = create_database_connection();
  if($_POST['flag'] != "")
    $stmt = $link->prepare($query1);
  else
    $stmt = $link->prepare($query);
  $stmt->bindParam(":int_title", $_POST['int_title']);
  $stmt->bindParam(":roundid", $_POST['round_id']);
  $stmt->bindParam(":pid", $_POST['id']);
  $stmt->bindParam(":mark", $_POST['mark']);
  $stmt->bindParam(":comments", $_POST['comments']);
  $stmt->execute();
  database_destroy($link);

}
function get_marks() {
  
    //geting participant details
    $round = get_rounds(); 
    $names = get_names();
    $mark_arr = array();
    foreach($names as $name){
      foreach($round as $rounds){
        $mark_arr[$name['id']][$rounds['id']] = 0;
      }
    }
    $query = "SELECT m.mark,p.id,rd.id as rid FROM participant as p LEFT JOIN round_marks as m ON p.id = m.participant_id LEFT JOIN round_details as rd ON rd.id = m.round_id";
    $link = create_database_connection();
    $stmt = $link->prepare($query);      
    $result=array();
    $stmt->execute();
    database_destroy($link);
    while($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
        $result[]=$row;
    }
    foreach($result as $res){
      $mark_arr[$res['id']][$res['rid']] = $res['mark'];
    }
    return $mark_arr;
     
}
function get_details(){

  $query = "SELECT * FROM round_marks WHERE round_id=:rid AND participant_id=:pid";
  $link = create_database_connection();
  $stmt = $link->prepare($query);
  $stmt->bindParam(":rid", $_GET['roundid']);
  $stmt->bindParam(":pid", $_GET['id']);     
  $result=array();
  $stmt->execute();
  $row = $stmt->fetch(PDO::FETCH_ASSOC);
  return $row;

}


?>